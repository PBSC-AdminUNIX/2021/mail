# Documentación mail
[ToC]
## Documentación Postfix
Postfix es un servidor de correo electrónico de open-source, este fue lanzado bajo una licencia pública de IBM 1.0 y ahora también está disponible bajo una licencia pública de Eclipse 2.0. Para su instalación en un sistema Centos 7 se necesita lo siguiente:
### SASL - saslauthd server (Cyrus SASL)
saslauthd también llamado saslauthd server es un demonio que permite el manejo de solicitudes de autenticación de texto sin formato por la biblioteca SASL. Este es el proceso de instalación y configuración en un sistema Centos 7
#### Instalación SASL
```
sudo yum install cyrus-sasl
sudo yum install cyrus-sasl-plain
```
#### Configuración SASL
```
sudo systemctl enable saslauthd
sudo systemctl start saslauthd
```
### Instalación de Postfix
```
sudo yum install postfix
```
### Configuración de Postfix
1. Modificar el archivo de configuración 
    ```
    sudo vi /etc/postfix/main.cf
    ```
    Archivo: [main.cf](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/Postfix/main.cf)
2. Modificar el archivo de configuración maestro
    ```
    sudo vi /etc/postfix/master.cf
    ```
    Archivo: [master.cf](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/Postfix/master.cf)
3. Crear el archivo de configuración smtpd para el demonio
    ```
    sudo mkdir /etc/postfix/sasl/
    sudo vi  /etc/postfix/sasl/smtpd.conf
    ```
    Archivo: [smtpd.conf](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/Postfix/smtpd.conf)
4. Crear los alias para el archivo /etc/aliases
    ```
    sudo vi /etc/aliases
    ```
    Archivo: [aliases](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/Postfix/aliases)
## Documentación Dovecot
### Instalación de Dovecot
```
sudo yum install dovecot -y
```
### Configuraración de Dovecot
1. Editar el archivo de configuración principal 
    - Descomentar línea para habilitar los protocolos 
    - Descomentar línea para habilitar la escucha de Dovecot en todas las interfaces
        ```
        sudo vi /etc/dovecot/dovecot.conf
        ```
    Archivo: [dovecot.conf](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/Dovecot/dovecot.conf)
2. Editar el archivo de configuración de correo 
    - Descomentar línea para habilitar la ubicación del buzón de correo
Nota: No usar mbox
        ```
        sudo vi /etc/dovecot/conf.d/10-mail.conf
        ```
    Archivo: [10-mail.conf](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/Dovecot/10-mail.conf)
3. Editar el archivo de configuración master
    - Establecer el nombre de usuario y grupo con el que se ejecuta el servidor de correo
        ```
        sudo vi /etc/dovecot/conf.d/10-master.conf
        ```
    Archivo: [10-master.conf](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/Dovecot/10-master.conf)
4. Editar el archivo de configuración.
    - Añadir la ruta de los certificados
        ```
        sudo vi /etc/dovecot/conf.d/10-ssl.conf
        ```
    Archivo: [10-ssl.conf](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/Dovecot/10-ssl.conf)
## Documentación Roundcube

### Instalación de Roundcube

1. Instalar epel 
    ```
    yum install epel-release y después yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
    ```
2. Instalar los siguientes paquetes
    ```
    yum install yum-utils
    ```
3. Instalar los siguientes paquetes para la base de datos
    ```
    yum install httpd mysql mariadb-server
    ```
4. Descargar los archivos de roundcube 
    ```
    wget -c https://github.com/roundcube/roundcubemail/releases/download/1.4.9/roundcubemail-1.4.9-complete.tar.gz
    ```
5. Descomprimir y mover los archivos al directorio /var/www/html
    ```
    tar xzf roundcubemail-1.4.9-complete.tar.gz  mv roundcubemail-1.4.9 /var/www/html/roundcubemail
    ```
6. Cambiar el propietario y los permisos de la carpeta /var/www/html
    ```
    chown -R apache:apache /var/www/html/roundcubemail 
    ```
7. Instalar certificado ssl para permitir sólo conexiones por https a roundcube
8. Crear el virtual host de roundcube en el archivo /etc/httpd/conf.d/roundcube.conf. El contenido del archivo se puede consultar en [roundcube.conf](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/RoundCube/roundcube.conf)
9. Reiniciar el servicio de apache 
    ```
    sudo service httpd restart
    ```
### Instalación de la base de datos
1. Iniciar el servicio de mariadb
    ```
    sudo systemctl start mariadb
    ```
2. Habilitar el servicio de mariadb
    ```
    sudo systemctl enable mariadb
    ```
3. Verificar el servicio de  mariadb
    ```
    sudo systemctl status mariadb
    ```
4. Instalar de forma segura mariadb
    ```
    sudo mysql_secure_installation
    ```
7. Crear un usuario y la base de datos
    ```
    mysql -u root -p
    CREATE DATABASE webmail /*!40101 CHARACTER SET utf8 COLLATE utf8_general_ci */;
    GRANT ALL PRIVILEGES ON webmail.* TO roundcube@localhost IDENTIFIED BY '********';
    FLUSH PRIVILEGES;
    exit;
    ```
8. Cargar los datos necesarios de roundcube
    ```
    mysql -u root -p webmail < /var/www/html/roundcubemail/SQL/mysql.initial.sql
    ```
### Configuación de Roundcube
1. Para realizar la configuración de roundcube se accede a la siguiente liga http://mail.planbecarios15.cf/installer  
2. Comprobar que las configuraciones  tengan el status: OK

Al terminar se puede observar un archivo en /var/www/html/roundcubemail/config/config.inc.php donde estará la configuración realizada
Archivo: [config.inc.php](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/RoundCube/config.inc.php)

## Documentación LDAP 
### Instalación de LDAP
```
sudo yum install nss-pam-ldapd openssl nscd
```
### Configuración de LDAP
1. Configurar el archivo /etc/nslcd.conf con los datos proporcionados por [directory](https://gitlab.com/PBSC-AdminUNIX/2021/directory)
    ```
    sudo vi /etc/nslcd.conf
    ```
    Archivo: [nslcd.conf](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/LDAP/nslcd.conf)
2. Configurar nslcd para que se inicie automáticamente
    ```
    systemctl enable nslcd
    systemctl restart nslcd
    ```
3. Actualizar los archivos /etc/nsswitch.conf y /etc/pam.d/, esto se hará mediante authconfig
    ```
    authconfig --updateall --enableldap --enableldapauth
    ```
4. Configurar nscd para que se inicie automáticamente
    ```
    systemctl restart nscd
    systemctl enable nscd
    ```
