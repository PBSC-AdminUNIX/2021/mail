# Proyecto Email

<img width="300" alt="screen shot 2018-12-16 at 10 32 38 am" src="https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/raw/main/img/mail.png">


## Instructores
 * Andrés Leonardo Hernández Bermúdez
 * Dante Erik Santiago Rodríguez Pérez
 * Enrique Tezozomoc Perez Campos


## Integrantes

* Colín López Sofía - *(`sofia.colin@bec.cert.unam.mx`)*[llave pública](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/files/id_scl_rsa.pub).
* Hernández Chávez Jorge Argenis - *(`jorge.hernandez@bec.cert.unam.mx`)* [llave pública](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/files/certMail.pub).
* Martinez Hernandez Luis Eduardo - *(`Luis.Martinez@bec.cert.unam.mx`)* [llave pública](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/files/id_rsa.pub).
* Olivera Trejo Luis David - *(`Luis.Olivera@bec.cert.unam.mx`)* [llave pública](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/files/luisolivera_id_rsa.pub)
* Salcedo Ramos Carlos Uriel - *(`carlos.salcedo@bec.cert.unam.mx`)* [llave pública](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/files/mailkey.pub).

# Como conectarse al servidor
https://mail.planbecarios15.cf/ 

Para conectarse al servidor de correo desde cualquier cliente se deben de introducir los siguientes datos:
### Incoming server settings
- Username -> su nombre de usuario que el equipo directory creo
- Password
- Servidor -> mail.planbecarios15.cf
- Puerto -> 993
- Seguridad -> SSL/TLS

### Outcoming server settings
- Username -> su nombre de usuario que el equipo directory creo
- Password
- Servidor -> mail.planbecarios15.cf
- Puerto -> 465
- Seguridad -> SSL/TLS

# Instrucciones
Instrucciones para la creacion del proyecto de mail. [Proyecto](https://gitlab.com/PBSC-AdminUNIX/2021/mail/-/blob/main/proyecto.md).
